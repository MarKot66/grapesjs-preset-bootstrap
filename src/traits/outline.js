export default (editor,config ={}) => {
    editor.TraitManager.addType('outline', {
        events:{
          'keyup': 'onChange',  // trigger parent onChange method on keyup
        },
      
        /**
        * Returns the input element
        * @return {HTMLElement}
        */
        getInputEl: function() {
          if (true) {
            var input = document.createElement('INPUT');
            input.setAttribute('type', 'checkbox');
          //  input.value = this.target.get('content');
            this.inputEl = input;
          }
          return this.inputEl;
        },
         /**
         * Triggered when the value of the model is changed
        */
        onValueChange: function () {
            console.log(this.target)
            this.target.set('content', this.model.get('value'));
        }
    });
}