export default (editor,config ={}) => {
    editor.TraitManager.addType('funneltarget', {
        events:{
          'keyup': 'onChange',  // trigger parent onChange method on keyup
        },
      
        /**
        * Returns the input element
        * @return {HTMLElement}
        */
        getInputEl: function() {
          if (!this.inputEl) {
            var btn = document.createElement('BUTTON');
            btn.innerHTML = 'Funnel-Ziel';
            btn.className = 'btn btn-primary btn-sm';
            //input.value = this.target.get('content');
            this.inputEl = btn;
          }
          return this.inputEl;
        },
         /**
         * Triggered when the value of the model is changed
        */
        onValueChange: function () {
           // console.log(this.target)
           // this.target.set('content', this.model.get('value'));
        }
    });
}