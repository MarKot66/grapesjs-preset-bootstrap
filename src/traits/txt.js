export default (editor,config ={}) => {
    editor.TraitManager.addType('txt', {
        events:{
          'keyup': 'onChange',  // trigger parent onChange method on keyup
        },
      
        /**
        * Returns the input element
        * @return {HTMLElement}
        */
        getInputEl: function() {
          if (!this.inputEl) {
            var input = document.createElement('input');
            input.setAttribute('type', 'text');
            input.value = this.target.get('content');
            this.inputEl = input;
          }
          return this.inputEl;
        },
         /**
         * Triggered when the value of the model is changed
        */
        onValueChange: function () {
            console.log(this.target)
            this.target.set('content', this.model.get('value'));
        }
    });
}