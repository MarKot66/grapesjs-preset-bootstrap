export default {
    type: 'select',
    label: 'Typ',
    name: 'btn-class',
    changeProp: 1,
    options: [
      {value: 'btn-primary', name: 'Primary'},
      {value: 'btn-secondary', name: 'Secondary'},
      {value: 'btn-success', name: 'Success'},
      {value: 'btn-danger', name: 'Danger'},
      {value: 'btn-warning', name: 'Warning'},
      {value: 'btn-info', name: 'Info'},
      {value: 'btn-light', name: 'Light'},
      {value: 'btn-dark', name: 'Dark'},
      {value: 'btn-link', name: 'Link'},
    ]
  }

  