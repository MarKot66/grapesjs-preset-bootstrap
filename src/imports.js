export default (editor, config = {}) => {
    let cc = editor.Canvas.getConfig();

    // Import Bootstrap Framework
    cc.scripts = ['https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js'];
    cc.styles = ['https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css','vendor/fontawesome-free/css/all.min.css','css/grayscale.min.css-o'];
  }