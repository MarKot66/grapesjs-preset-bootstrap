import target from './traits/target';
import txt from './traits/txt';
import outline from './traits/outline';

export default (editor, config = {}) => {
    target(editor, config);
    txt(editor, config);
    outline(editor, config);
}



