import  bs_button_class from '../traits/bs_button_class';
import  target from '../traits/target';

export default (editor, config = {}) => {
    var domComps = editor.DomComponents;
    var dType = domComps.getType('default');
    var dModel = dType.model;
    var dView = dType.view;
    
    domComps.addType('button', {
        model: dModel.extend({
          init() {
              this.listenTo(this, 'change:btn-class', this.updateClasses);
             // this.listenTo(this, 'change:txt', this.updateTxt);
            },
      
            updateClasses() {
              var classes = this.getClasses();
              var optionClasses = bs_button_class.options.map((o)=>{return o.value;});
              this.removeClass(optionClasses)
              this.addClass(this.get('btn-class'));
            },

            defaults: Object.assign({}, dModel.prototype.defaults, {
                traits: [
                    {  
                        type: 'txt',
                        label: 'Text',
                        name: 'txt',
                        changeProp: 1
                    },
                    bs_button_class,
                    {
                        type: 'outline',
                        label: 'Outline?',
                        name: 'outline',
                        changeProp: 1
                    },
                  /*  {  
                        type: 'funneltarget',
                        label: 'Ziel',
                        name: 'target',
                        changeProp: 1
                    },*/
                ],
              }),
            }, {
              isComponent: function(el) {
                if(el.tagName == 'BUTTON'){
                  return {type: 'button'};
                }
              }
        }),
        view: dView.extend({
            events: {
              // If you want to bind the event to children elements
              // 'click .someChildrenClass': 'methodName',
             /* click: function(){
                console.log(editor.Commands.getActive());
                editor.stopCommand('open-blocks');
                editor.stopCommand('sw-visibility');
                editor.runCommand('open-tm');
             
              },*/
              dblclick: function(){
                console.log(editor.Commands.getActive());
                editor.stopCommand('open-blocks');
                editor.stopCommand('sw-visibility');
                editor.runCommand('open-tm');
             
              }
            },
        
            // It doesn't make too much sense this method inside the component
            // but it's ok as an example
            randomHex: function() {
              return '#' + Math.floor(Math.random()*16777216).toString(16);
            },
        
          }),
      });
        
        
  }
