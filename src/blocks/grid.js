export default (editor, config = {}) => {
    const bm = editor.BlockManager;
    console.log(bm)
    bm.add('bs-grid', {
      //label: 'BS Button',
      label: `<div class="prevImgWrapper" style="text-align:center;padding-top:10px">
      <img class="prevImg" style="max-width:100%" src="/assets/grid.png">
      <div class="my-label-block">Grid</div>
    </div>`,
      category: {id: 'bootstrap', label: 'Bootstrap'},
      attributes: {
        text: 'Insert h1 block'
      },
      //attributes: {class: 'fa fa-mouse-pointer'},
      content: `<div class="container">
      <div class="row">
        <div class="col">
          1 of 2
        </div>
        <div class="col">
          2 of 2
        </div>
      </div>
      <div class="row">
        <div class="col">
          1 of 3
        </div>
        <div class="col">
          2 of 3
        </div>
        <div class="col">
          3 of 3
        </div>
      </div>
    </div>`,
      render: ({ model, className }) => `<div class="${className}__my-wrap">${model.get('label')}</div>`,
    });
  
  
  }
