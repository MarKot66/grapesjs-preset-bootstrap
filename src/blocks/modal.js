export default (editor, config = {}) => {
    const bm = editor.BlockManager;
    console.log(bm)
    bm.add('bs_modal',{
      //label: 'BS Button',
      label: `<div class="prevImgWrapper" style="text-align:center;padding-top:10px">
      <img class="prevImg" style="max-width:100%" src="/assets/modal.png">
      <div class="my-label-block">Modal</div>
    </div>`,
      category: {id: 'bootstrap', label: 'Bootstrap'},
      attributes: {
        text: 'Insert h1 block'
      },
      //attributes: {class: 'fa fa-mouse-pointer'},
      content: `<div><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Launch demo modal
      </button>
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              ...
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
      </div></div>`,
      render: ({ model, className }) => `<div class="${className}__my-wrap">${model.get('label')}</div>`,
    });
  
  }
  