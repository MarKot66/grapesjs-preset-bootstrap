export default (editor, config = {}) => {
    const bm = editor.BlockManager;
    console.log(bm)
    bm.add('card',{
      //label: 'BS Button',
      label: `<div class="prevImgWrapper" style="text-align:center;padding-top:10px">
      <img class="prevImg" style="max-width:100%" src="/assets/card.png">
      <div class="my-label-block">Card</div>
    </div>`,
      category: {id: 'bootstrap', label: 'Bootstrap'},
      attributes: {
        text: 'Insert h1 block'
      },
      //attributes: {class: 'fa fa-mouse-pointer'},
      content: `<div class="card" style="width: 18rem;">
      <img class="card-img-top" src="..." alt="Card image cap">
      <div class="card-body">
        <h5 class="card-title">Card title</h5>
        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
        <a href="#" class="btn btn-primary">Go somewhere</a>
      </div>
    </div>`,
      render: ({ model, className }) => `<div class="${className}__my-wrap">${model.get('label')}</div>`,
    });
  
  }
  
  

