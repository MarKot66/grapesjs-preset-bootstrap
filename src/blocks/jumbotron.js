export default (editor, config = {}) => {
  const bm = editor.BlockManager;
  console.log(bm)
  bm.add('jumbotron',{
    //label: 'BS Button',
    label: `<div class="prevImgWrapper" style="text-align:center;padding-top:10px">
    <img class="prevImg" style="max-width:100%" src="/assets/jumbotron.png">
    <div class="my-label-block">Jumbotron</div>
  </div>`,
    category: {id: 'bootstrap', label: 'Bootstrap'},
    attributes: {
      text: 'Insert h1 block'
    },
    //attributes: {class: 'fa fa-mouse-pointer'},
    content: `<div class="jumbotron">
    <h1 class="display-4">Hello, world!</h1>
    <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
    <hr class="my-4">
    <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
    <p class="lead">
      <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
    </p>
  </div>`,
    render: ({ model, className }) => `<div class="${className}__my-wrap">${model.get('label')}</div>`,
  });

}

