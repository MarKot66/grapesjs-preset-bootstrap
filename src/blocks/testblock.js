var testProp = 'Marco';

export default (editor, config = {}) => {
    editor.BlockManager.add('test-block', {
        label: 'Test block',
        category: {id: 'bootstrap', label: 'Bootstrap'},
        attributes: {class: 'fa fa-text'},
        content: {
        myModelPropName: testProp,
          // script: "alert('Hi'); console.log('the element', this)",
          script: function () {
            alert('Hi {[ myModelPropName ]}');
            console.log('the element', this);

            // ***** Custom dynamic DEP

            if (typeof CoolSliderJS == 'undefined') {
                var script = document.createElement('script');
                script.onload = initMySLider;
                script.src = 'https://.../coolslider.min.js';
                document.body.appendChild(script);
              }
          },
          // Add some style just to make the component visible
          style: {
            width: '100px',
            height: '100px',
            'background-color': 'red',
          }
        }
      });
};