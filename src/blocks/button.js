export default (editor, config = {}) => {
    const bm = editor.BlockManager;
    console.log(bm)
    bm.add('bs-btn', {
      //label: 'BS Button',
      label: `<div style="text-align:center;padding-top:20px"><button style="font-size:1em;" class="btn btn-primary btn-sm">Text</button></div><p>Button</p>`,
      category: {id: 'bootstrap', label: 'Bootstrap'},
      attributes: {
        text: 'Insert h1 block'
      },
      //attributes: {class: 'fa fa-mouse-pointer'},
      content: `<button type="button" class="btn btn-primary">Button</button>`,
      render: ({ model, className }) => `<div class="${className}__my-wrap">${model.get('label')}</div>`,
    });

  }