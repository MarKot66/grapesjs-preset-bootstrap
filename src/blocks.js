import button from './blocks/button';
import jumbotron from './blocks/jumbotron';
import card from './blocks/card';
import modal from './blocks/modal';
import grid from './blocks/grid';
import testblock from './blocks/testblock';

export default (editor, config = {}) => {
  var bm = editor.BlockManager;

  button(editor, config);
  jumbotron(editor, config);
  card(editor, config);
  modal(editor, config);
  grid(editor, config);

  testblock(editor,config);
 // block




  bm.add('quote', {
    label: 'Quote',
    category: 'Basic',
    attributes: { class: 'fa fa-quote-right' },
    content: `<blockquote class="quote">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ipsum dolor sit
      </blockquote>`
  });

 bm.add('text-basic', {
    category: 'Basic',
    label: 'Text section',
    attributes: { class: 'gjs-fonts gjs-f-h1p' },
    content: `<section class="bdg-sect">
      <h1 class="heading">Insert title here</h1>
      <p class="paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
      </section>`
});
}
